package com.wyd.wenyongdaCredit.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.lang.Snowflake;
import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.wyd.wenyongdaCredit.mapper.CustomerMapper;
import com.wyd.wenyongdaCredit.mapper.MetricDefMapper;
import com.wyd.wenyongdaCredit.model.dto.CustomerDTO;
import com.wyd.wenyongdaCredit.model.dto.MetricDefDTO;
import com.wyd.wenyongdaCredit.model.entity.CustomerEntity;
import com.wyd.wenyongdaCredit.model.entity.MetricDefEntity;
import com.wyd.wenyongdaCredit.service.ICustomerService;
import com.wyd.wenyongdaCredit.service.IMetricDefService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class MetricDefServiceImpl extends ServiceImpl<MetricDefMapper, MetricDefEntity> implements IMetricDefService {

    private static final Logger log = LoggerFactory.getLogger(MetricDefServiceImpl.class);

    @Override
    public List<MetricDefDTO> selectMetricDef(Integer metricKind) {
        List<MetricDefEntity> metricDefEntityList = this.baseMapper.selectList(
                new QueryWrapper<MetricDefEntity>().eq("metric_kind", metricKind));
        List<MetricDefDTO> metricDefDTOList = new ArrayList<>();
        MetricDefDTO metricDefDTO = null;
        for (MetricDefEntity metricDefEntity : metricDefEntityList) {
            metricDefDTO = new MetricDefDTO();
            BeanUtil.copyProperties(metricDefEntity, metricDefDTO);
            metricDefDTOList.add(metricDefDTO);
        }
        return metricDefDTOList;
    }

    @Override
    public Integer addMetricDef(MetricDefDTO metricDefDTO) {
        if (ObjectUtil.isNotNull(metricDefDTO)) {
            Snowflake snowflake = IdUtil.createSnowflake(1, 1);
            MetricDefEntity metricDef = new MetricDefEntity();
            BeanUtil.copyProperties(metricDefDTO, metricDef);
            return this.baseMapper.insert(metricDef);
        } else {
            return 0;
        }
    }

    @Override
    public Integer updateMetricDef(MetricDefDTO metricDefDTO) {
        if (ObjectUtil.isNotNull(metricDefDTO)) {
            return this.baseMapper.update(new MetricDefEntity(), metricDefUpdateWrapper(metricDefDTO));
        } else {
            return 0;
        }
    }

    @Override
    public Integer deleteMetricDef(List<String> ids) {
        if (ObjectUtil.isNotNull(ids) && ids.size() > 0) {
            return this.baseMapper.deleteBatchIds(ids);
        } else {
            return 0;
        }
    }

    private UpdateWrapper<MetricDefEntity> metricDefUpdateWrapper(MetricDefDTO metricDefDTO) {
        if (ObjectUtil.isNotNull(metricDefDTO)) {
            UpdateWrapper<MetricDefEntity> updateWrapper = new UpdateWrapper<>();
            if (StrUtil.isNotBlank(String.valueOf(metricDefDTO.getMetricCode()))) {
                updateWrapper.eq("metric_code", metricDefDTO.getMetricCode());
            }
            if (ObjectUtil.isNotNull(metricDefDTO.getMetricKind())) {
                updateWrapper.set("metric_kind", metricDefDTO.getMetricKind());
            }
            if (StrUtil.isNotBlank(metricDefDTO.getMetricName())) {
                updateWrapper.set("metric_name", metricDefDTO.getMetricName());
            }
            if (ObjectUtil.isNotNull(metricDefDTO.getMetricType())) {
                updateWrapper.set("metric_type", metricDefDTO.getMetricType());
            }
            if (ObjectUtil.isNotNull(metricDefDTO.getWeight())) {
                updateWrapper.set("weight", metricDefDTO.getWeight());
            }
            if (ObjectUtil.isNotNull(metricDefDTO.getIsEnabled())) {
                updateWrapper.set("is_enabled", metricDefDTO.getIsEnabled());
            }
            return updateWrapper;
        }
        return null;
    }

}
