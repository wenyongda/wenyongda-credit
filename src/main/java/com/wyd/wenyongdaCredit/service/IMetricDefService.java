package com.wyd.wenyongdaCredit.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.github.pagehelper.PageInfo;
import com.wyd.wenyongdaCredit.model.dto.CustomerDTO;
import com.wyd.wenyongdaCredit.model.dto.MetricDefDTO;
import com.wyd.wenyongdaCredit.model.entity.CustomerEntity;
import com.wyd.wenyongdaCredit.model.entity.MetricDefEntity;

import java.util.List;

public interface IMetricDefService extends IService<MetricDefEntity> {

    List<MetricDefDTO> selectMetricDef(Integer metricKind);

    Integer addMetricDef(MetricDefDTO metricDefDTO);

    Integer updateMetricDef(MetricDefDTO metricDefDTO);

    Integer deleteMetricDef(List<String> ids);
}
