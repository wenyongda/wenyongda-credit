package com.wyd.wenyongdaCredit.controller;


import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.convert.Convert;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.github.pagehelper.PageInfo;
import com.wyd.wenyongdaCredit.commons.util.ResultEnum;
import com.wyd.wenyongdaCredit.commons.util.ResultUtil;
import com.wyd.wenyongdaCredit.model.dto.MetricDefDTO;
import com.wyd.wenyongdaCredit.model.dto.RatingTemplateDTO;
import com.wyd.wenyongdaCredit.model.entity.RatingTemplateEntity;
import com.wyd.wenyongdaCredit.service.IMetricDefService;
import com.wyd.wenyongdaCredit.service.IRatingTemplateService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;


/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author wenyongda
 * @since 2021-10-18
 */
@Api(tags = "用户中心")
@RestController
@RequestMapping("/metric-def")
@RefreshScope
public class MetricDefController {
    private static final Logger log = LoggerFactory.getLogger(MetricDefController.class);

    private IMetricDefService metricDefService;

    @Autowired
    public void setMetricDefService(IMetricDefService metricDefService) {
        this.metricDefService = metricDefService;
    }


    @ApiOperation("添加用户")
    @PostMapping("/save")
    public ResultUtil addMetricDef(@RequestBody Map<String, Object> reqParams) {
        if (ObjectUtil.isNotNull(reqParams.get("metricCode")) && ObjectUtil.isNotNull(reqParams.get("templateId"))
            && ObjectUtil.isNotNull(reqParams.get("metricKind")) &&  ObjectUtil.isNotNull(reqParams.get("metricName"))
        && ObjectUtil.isNotNull(reqParams.get("metricType"))) {

            Integer res = this.metricDefService.addMetricDef(BeanUtil.fillBeanWithMap(reqParams, new MetricDefDTO(), false));
            if (res > 0) {
                return ResultUtil.ok();
            } else {
                return ResultUtil.error(ResultEnum.UNKNOWN_ERROR);
            }
        } else {
            return ResultUtil.error(ResultEnum.USERNAME_OR_NICKNAME_IS_EMPTY);
        }
    }

    @ApiOperation("分页查询用户列表")
    @PostMapping("/getAll")
    public ResultUtil selectMetricDef(@RequestBody Map<String, Object> reqParams) {
        List<MetricDefDTO> metricDefDTOList = this.metricDefService.selectMetricDef((Integer) reqParams.get("metricKind"));
        return ResultUtil.ok().data("records", metricDefDTOList);
    }


    //@ApiOperation("更新用户")
    @PutMapping("/update")
    public ResultUtil updateMetricDef(@RequestBody Map<String, Object> reqParams) {
        if (ObjectUtil.isNotNull(reqParams.get("metricCode"))) {
            MetricDefDTO metricDefDTO = BeanUtil.fillBeanWithMap(reqParams, new MetricDefDTO(), false);
            Integer updatemetricDefRes = this.metricDefService.updateMetricDef(metricDefDTO);
            if (updatemetricDefRes > 0) {
                return ResultUtil.ok();
            } else {
                return ResultUtil.error(ResultEnum.DICT_ENTRY_ERROR_OR_UNKNOWN);
            }
        } else {
            return ResultUtil.error(ResultEnum.USER_ID_IS_EMPTY);
        }
    }

    @ApiOperation("删除用户")
    @DeleteMapping("/delete")
    public ResultUtil deleteMetricDef(@RequestBody Map<String, Object> reqParams) {
        if (ObjectUtil.isNotNull(reqParams.get("ids"))) {
            List<String> ids = Convert.toList(String.class, reqParams.get("ids"));
            if (this.metricDefService.deleteMetricDef(ids) > 0 ) {
                return ResultUtil.ok();
            } else {
                return ResultUtil.error(ResultEnum.UNKNOWN_ERROR);
            }
        } else {
            return ResultUtil.error(ResultEnum.USER_ID_IS_EMPTY);
        }
    }

    private RatingTemplateDTO ratingTemplateDTOIfHasStrBlankToNull(Map<String, Object> reqParams) {
        RatingTemplateDTO ratingTemplateDTO = new RatingTemplateDTO();
        if (StrUtil.isNotBlank((String) reqParams.get("id"))) {
            Long id = Long.valueOf((String) reqParams.get("id"));
            ratingTemplateDTO.setId(id);
        }
        String templateName = (String) reqParams.get("templateName");
        ratingTemplateDTO.setTemplateName(templateName);

        if (ObjectUtil.isNotNull(reqParams.get("templateType"))) {
            ratingTemplateDTO.setTemplateType((Integer) reqParams.get("templateType"));
        }
        //Integer idType = Integer.valueOf(reqParams.get("idType").toString());
        //customerDTO.setIdType(idType);
        //if (StrUtil.isNotBlank((String) reqParams.get("id"))) {
        //    String id = (String) reqParams.get("id");
        //    customerDTO.setId(id);
        //}
        //if (StrUtil.isNotBlank((String) reqParams.get("customerName"))) {
        //    String customerName = (String) reqParams.get("customerName");
        //    customerDTO.setCustomerName(customerName);
        //}
        //if (StrUtil.isNotBlank((String) reqParams.get("idNumber"))) {
        //    String idNumber = (String) reqParams.get("idNumber");
        //    customerDTO.setIdNumber(idNumber);
        //}
        return ratingTemplateDTO;
    }
}
