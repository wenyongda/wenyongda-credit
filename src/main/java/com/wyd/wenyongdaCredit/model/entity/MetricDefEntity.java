package com.wyd.wenyongdaCredit.model.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.Accessors;
import org.springframework.data.annotation.Id;
import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.util.Date;

@Data
@EqualsAndHashCode
@ToString
@Accessors(chain = true)
@TableName("t_metric_def")
public class MetricDefEntity {

    @Id
    @TableId(value = "metric_code", type = IdType.INPUT)
    private String metricCode;

    private Long templateId;

    private Integer metricKind;

    private String metricName;

    private Integer metricType;

    private BigDecimal weight;

    @TableField(value = "create_time", fill = FieldFill.INSERT)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    @TableField(value = "update_time", fill = FieldFill.UPDATE)// 出参格式化
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")// 入参格式化
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updateTime;

    @TableField(value = "is_deleted", fill = FieldFill.INSERT)
    private Integer isDeleted;

    @TableField(value = "is_enabled", fill = FieldFill.INSERT)
    private Integer isEnabled;

    @Version
    @TableField(value = "version")
    private Integer version;
}
