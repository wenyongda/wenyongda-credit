package com.wyd.wenyongdaCredit.model.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.math.BigDecimal;

@Data
@EqualsAndHashCode
@ToString
@Accessors(chain = true)
public class MetricDefDTO {

    private String metricCode;

    private Long templateId;

    private Integer metricKind;

    private String metricName;

    private Integer metricType;

    private BigDecimal weight;

    private Integer isEnabled;
}
