package com.wyd.wenyongdaCredit.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wyd.wenyongdaCredit.model.entity.MetricDefEntity;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface MetricDefMapper extends BaseMapper<MetricDefEntity> {

}
